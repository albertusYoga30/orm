const { books } = require('./models');

const query = { where: { id: 1 } };

books
  .update(
    {
      judul: 'semakin tipis',
    },
    query
  )
  .then(() => {
    console.log('Books update success');
    process.exit();
  })
  .catch((err) => {
    console.error('update failed');
  });
